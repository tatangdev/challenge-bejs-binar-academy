const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

function header() {
  console.log("========================================")
  console.log("              CHALLANGE-02              ")
  console.log("      PROGRAM INPUT NILAI MAHASISWA     ")
  console.log("========================================")
  console.log("Inputkan nilai, 'q' untuk keluar program")
}

class Mahasiswa {
  constructor(nilai) {
    this.nilai = nilai
  }

  mean() {
    const rataRata = (this.nilai.reduce((total, nilai) => total + nilai, 0)) / this.nilai.length
    console.log(`Rata-rata             : ${rataRata}`)
  }

  rank() {
    const ranking = this.nilai.sort((a, b) => b - a)
    const juara1 = ranking[0]
    const juruKunci = ranking[ranking.length - 1]

    console.log(`Ranking dari terbesar : ${ranking}`)
    console.log(`Nilai terbesar        : ${juara1}`)
    console.log(`Nilai terendah        : ${juruKunci}`)
  }

  kelulusan() {
    const lulus = []
    const tidakLulus = []

    this.nilai.forEach(n => {
      if (n >= 60) {
        lulus.push(n)
      } else {
        tidakLulus.push(n)
      }
    })

    console.log(`Total banyak nilai    : ${this.nilai.length}`)
    console.log(`Lulus                 : ${lulus.length}`)
    console.log(`Tidak lulus           : ${tidakLulus.length}`)
  }

  cariNilai() {
    let nilai90 = []
    let nilai100 = []

    this.nilai.forEach(n => {
      if (n == 90) {
        nilai90.push(+n)
      }

      if (n == 100) {
        nilai100.push(+n)
      }
    })

    if (nilai90.length == 0) {
      nilai90 = 'Tidak ditemukan'
    }

    if (nilai100.length == 0) {
      nilai100 = 'Tidak ditemukan'
    }

    console.log(`Siswa nilai 90        : ${nilai90}\nSiswa nilai 100       : ${nilai100}`)
  }
}

function input() {
  return new Promise(resolve => {
    readline.question('', data => {
      return resolve(data)
    })
  })
}

async function inputNilai() {
  const nilai = []
  let n = await input()

  if (n == 'q') {
    readline.close()
  } else {
    if (n != 'q' && n >= 0 && n <= 100) {
      nilai.push(+n)
      while (n != 'q') {
        n = await input()
        if (n != 'q' && n >= 0 && n <= 100) {
          nilai.push(+n)
        } else if (n == 'q') {
          readline.close()
        } else {
          console.log('Inputan tidak valid, ulangi!')
          setTimeout(() => {
            inputNilai()
          })
        }
      }
    } else {
      if (n == 'q') {
        readline.close()
      } else {
        console.log('Inputan tidak valid, ulangi!')
        setTimeout(() => {
          console.clear()
          main()
        }, 1500)
      }
    }
    /*while (n != 'q') {
      n = await input()
      if (n != 'q' && n >= 0 && n <= 100) {
        nilai.push(+n)
      } else {
        console.log('Inputan tidak valid!')
        inputNilai()
      }
    }*/
  }

  return nilai
}

async function main() {
  header()
  const nilai = await inputNilai()

  if (nilai.length != 0) {
    console.log("========================================")
    const mahasiswa = new Mahasiswa(nilai)
    mahasiswa.mean()
    mahasiswa.rank()
    mahasiswa.kelulusan()
    mahasiswa.cariNilai()
    readline.close()
  }
}

main()
