const { Collection } = require('postman-collection')
const fs = require('fs')
const items = require('./items')

// Collection
const postmanCollection = new Collection({
  info: {
    name: 'challange-05'
  },
  item: []
})

postmanCollection.items.add(items.auth.register)
postmanCollection.items.add(items.auth.login)
postmanCollection.items.add(items.auth.logout)
postmanCollection.items.add(items.auth.changePass)
postmanCollection.items.add(items.user.update)
postmanCollection.items.add(items.user.destroy)

// Convert to JSSON
const collectionJSON = postmanCollection.toJSON()

// Export to file
fs.writeFile('./collection.json', JSON.stringify(collectionJSON, null, 2), (err) => {
  if (err) throw err
  console.log('file saved')
})
