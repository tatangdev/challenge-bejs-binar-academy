require('dotenv').config()
const { Header } = require('postman-collection')
const { TOKEN } = process.env

const rawHeaderString = `Authorization:${TOKEN}\nContent-Type:application/json\ncache-control:no-cache\n`
const rawHeader = Header.parse(rawHeaderString)
const newHeader = rawHeader.map(h => new Header(h))

module.exports = { newHeader }
