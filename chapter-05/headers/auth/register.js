const { Header } = require('postman-collection')

const rawHeaderString = 'Authorization:\nContent-Type:application/json\ncache-control:no-cache\n'
const rawHeader = Header.parse(rawHeaderString)
const newHeader = rawHeader.map(h => new Header(h))

module.exports = { newHeader }
