const update = 'update user endpoint'
const destroy = 'delete user endpoint'

module.exports = { update, destroy }
